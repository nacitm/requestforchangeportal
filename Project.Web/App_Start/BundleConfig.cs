﻿using System.Web.Optimization;

namespace Project.Web.App_Start
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.unobtrusive*",
                "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
                "~/Scripts/knockout-*"));

           /* bundles.Add(new ScriptBundle("~/bundles/workless").Include(""/*
               "~/Scripts/workless/application.js",
               "~/Scripts/workless/ie_font.js",
               "~/Scripts/workless/jquery.js",
               "~/Scripts/workless/modernizr.js",
               "~/Scripts/workless/plugins.js"
               ));*/

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
               "~/Scripts/bs/boot*"               
            ));

            /*
                  "~/Content/css/alerts.css",
                  "~/Content/css/application.css",
                  "~/Content/css/breadcrumbs.css",
                  "~/Content/css/button.css",
                  "~/Content/css/font.css",
                  "~/Content/css/forms.css",
                  "~/Content/css/helpers.css",
                  "~/Content/css/icons.css",
                  "~/Content/css/pageination.css",
                  "~/Content/css/plugins.css",
                  "~/Content/css/print.css",
                  "~/Content/css/scaffolding.css",
                  "~/Content/css/tables.css",
                  "~/Content/css/tabs.css",
                  "~/Content/css/typography.css",
                  "~/Content/css/workless.css",
                  
             */

            // All CSS styles
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/css/monkeypatch.css",
                  "~/Content/css/bs/bootstrap.css"));
        }
    }
}