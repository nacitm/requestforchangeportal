﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers
{
    public class IndexController : Controller
    {  
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetUsers ()
        {
            return Json("name", JsonRequestBehavior.AllowGet);
        }

    }
}
